# Ansible Role: Kubectx

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kubectx/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kubectx/-/commits/main)

This role installs [Kubectx](https://github.com/ahmetb/kubectx) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubectx_version: latest # v0.9.3 if you want a specific version
    kubectx_arch: x86_64 # x86_64, arm64, armhf, armv7, ppc64le
    kubens_arch: x86_64 # x86_64, arm64, armhf, armv7, ppc64le

    setup_dir: /tmp

    kubectx_bin_path: /usr/local/bin/kubectx
    kubens_bin_path: /usr/local/bin/kubens
    kubectx_repo_path: https://github.com/ahmetb/kubectx/releases/download

This role can install the latest or a specific version. See [available kubectx releases](https://github.com/ahmetb/kubectx) and change this variable accordingly.

    kubectx_version: latest # v0.9.3 if you want a specific version

The path of the repo which kubectx will be downloaded.

    kubectx_repo_path: https://github.com/ahmetb/kubectx/download

The path to the home kubectx and kubens directory.

    kubectx_bin_path: /usr/local/bin/kubectx
    kubens_bin_path: /usr/local/bin/kubens

The location where the kubectx and kubens binary will be installed.

The path to the home kubectx directory.

    kubectx_arch: x86_64 # x86_64, arm64, armhf, armv7, ppc64le
    kubens_arch: x86_64 # x86_64, arm64, armhf, armv7, ppc64le

kubectx supports x86_64, arm64, armhf, armv7 and ppc64le CPU architectures, just change for the main architecture of your CPU

kubectx and kubens need a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kubectx

## License

MIT / BSD
